// Generics - Declaration-site Variance

//interface Source<out T> {
//  fun nextT(): T
//}
//
//fun demo(strs: Source<String>) {
//  val objects: Source<Any> = strs // This is OK, since T is an out-parameter
//  // ...
//}
//
//interface Comparable<in T> {
//  operator fun compareTo(other: T): Int
//}
//
//fun demo(x: Comparable<Number>) {
//  x.compareTo(1.0) // 1.0 has type Double, which is a subtype of Number
//  // Thus, you can assign x to a variable of type Comparable<Double>
//  val y: Comparable<Double> = x // OK!
//}



// Generics - Type Projections

//class Array<T>(val size: Int) {
//  operator fun get(index: Int): T { ... }
//  operator fun set(index: Int, value: T) { ... }
//}
//
//fun copy(from: Array<Any>, to: Array<Any>) {
//  assert(from.size == to.size)
//  for (i in from.indices)
//    to[i] = from[i]
//}
//
//fun main() {
//  val ints: Array<Int> = arrayOf(1, 2, 3)
//  val any = Array<Any>(3) { "" }
//  copy(ints, any)
////   ^ type is Array<Int> but Array<Any> was expected
//}


// Generics - Functions

//fun <T> singletonList(item: T): List<T> {
//  // ...
//}
//
//fun <T> T.basicToString(): String { // extension function
//  // ...
//}
//
//fun main() {
//  var l = singletonList<Int>(1)
//  l = singletonList(1)
//}


// Generics - Upper Bounds

//fun <T : Comparable<T>> sort(list: List<T>) {  ... }
//
//fun main() {
//  sort(listOf(1, 2, 3)) // OK. Int is a subtype of Comparable<Int>
//  sort(listOf(HashMap<Int, String>())) // Error: HashMap<Int, String> is not a subtype of Comparable<HashMap<Int, String>>
//}