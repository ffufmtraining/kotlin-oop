// Init Block 1
class User {
  init {
    print("Class instance is initialised.")
  }

  var loggedIn: Boolean = false
  val cantChangeValue = "Hi"

  fun logOn() {
    loggedIn = true
  }

  fun logOff() {
    loggedIn = false
  }
}


// Init Block 2
class MultiInit(name: String) {
  init {
    println("First initializer block that prints $name")
  }

  init {
    println("Second initializer block that prints ${name.length}")
  }
}

fun main(args: Array<String>) {
  var multiInit = MultiInit("Kotlin")
}