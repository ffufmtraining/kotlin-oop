// Companion Object 1

//class MyClass {
//  companion object Factory {
//    fun create(): MyClass = MyClass()
//  }
//}

//fun main() {
//  val instance = MyClass.create()
//}


// Companion Object 2

//class MyClass {
//  companion object { }
//}

//fun main() {
//  val x = MyClass.Companion
//}


// Companion Object 3

//class MyClass1 {
//  companion object Named { }
//}

//val x = MyClass1
//
//class MyClass2 {
//  companion object { }
//}

//fun main() {
//  val y = MyClass2
//}


// Companion Object 4

//interface Factory<T> {
//  fun create(): T
//}

//class MyClass {
//  companion object : Factory<MyClass> {
//    override fun create(): MyClass = MyClass()
//  }
//}

//fun main() {
//  val f: Factory<MyClass> = MyClass
//}