// Data Class 1
data class Person4(val name: String) {
  var age: Int = 0
}

fun run3() {
  val person1 = Person4("John")
  val person2 = Person4("John")
  person1.age = 10
  person2.age = 20
  println("person1 == person2: ${person1 == person2}")
  println("person1 with age ${person1.age}: $person1")
  println("person2 with age ${person2.age}: $person2")
}

// Data Class 2 - Copying
data class User2(val name: String = "", val age: Int = 0)
fun copy(name: String = "Jack", age: Int = 1) = User2("Jack", 1)

fun run4() {
  val jack = User2(name = "Jack", age = 1)
  println(jack)

  val olderJack = jack.copy(age = 2)
  print(olderJack)
}

// Data Class 3 - Destructuring Declarations
data class User3(val name: String = "", val age: Int = 0)

fun run5() {
  val jane = User3("Jane", 35)
  val (name, age) = jane
  println("$name, $age years of age")
}