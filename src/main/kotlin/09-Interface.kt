// Interface - Properties in Interfaces
interface MyInterface {
  val prop: Int // abstract

  val propertyWithImplementation: String
    get() = "foo"

  fun foo() {
    print(prop)
  }
}

class Child : MyInterface {
  override val prop: Int = 29
}

// Interface - Inheritance
interface Named {
  val name: String
}

interface Persons2 : Named {
  val firstName: String
  val lastName: String

  override val name: String get() = "$firstName $lastName"
}

interface Position {
  val name: String
}

data class Employees3(
  // implementing 'name' is not required
  override val firstName: String,
  override val lastName: String,
  val position: Position
) : Persons2


// Interface - Resolving Overriding Conflicts
interface A {
  fun foo() {
    print("A")
  }

  fun bar()
}

interface B {
  fun foo() {
    print("B")
  }

  fun bar() {
    print("bar")
  }
}

class C : A {
  override fun bar() {
    print("bar")
  }
}

class D : A, B {
  override fun foo() {
    super<A>.foo()
    super<B>.foo()
  }

  override fun bar() {
    super<B>.bar()
  }
}