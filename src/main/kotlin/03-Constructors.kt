// Primary Constructors 1
class InitOrderDemo(name: String) {
  val firstProperty = "First property: $name".also(::println)

  init {
    println("First initializer block that prints $name")
  }

  val secondProperty = "Second property: ${name.length}".also(::println)

  init {
    println("Second initializer block that prints ${name.length}")
  }
}

// Primary Constructors 2
class Customer(name: String) {
  val customerKey = name.uppercase()
}

// Primary Constructors 3
class Person(val firstName: String, val lastName: String, var isEmployed: Boolean = true)

// Secondary Constructors 1
class Persons(val name: String) {
  var children: MutableList<Persons> = mutableListOf()
  constructor(name: String, parent: Persons) : this(name) {
    parent.children.add(this)
  }
}

// Secondary Constructors 2
class Constructors {
  init {
    println("Init block")
  }

  constructor(i: Int) {
    println("Constructor $i")
  }
}

// Secondary Constructors 3
class DontCreateMe private constructor () { /*...*/ }