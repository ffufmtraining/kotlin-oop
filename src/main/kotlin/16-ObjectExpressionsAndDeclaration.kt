// Object Expressions

//fun main() {
//  val helloWorld = object {
//    val hello = "Hello"
//    val world = "World"
//    // object expressions extend Any, so `override` is required on `toString()`
//    override fun toString() = "$hello $world"
//  }
//
//  print(helloWorld)
//}

// Hello World


// Object Declaration

//object DataProviderManager {
//  fun registerDataProvider(provider: DataProvider) {
//    // ...
//  }
//
//  val allDataProviders: Collection<DataProvider>
//    get() = // ...
//}
//
//DataProviderManager.registerDataProvider(...)