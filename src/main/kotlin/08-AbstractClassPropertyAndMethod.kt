// Abstract Class, Property, and Method
// Concrete (Non Abstract) Properties
abstract class Vehicle(
  private val name: String,
  private val color: String,
  private val weight: Double) {

  abstract var maxSpeed: Double

  abstract fun start()
  abstract fun stop()

  fun displayDetails() {
    println("Name: $name, Color: $color, Weight: $weight, Max Speed: $maxSpeed")
  }
}

class Cars(name: String,
          color: String,
          weight: Double,
          override var maxSpeed: Double): Vehicle(name, color, weight) {

  override fun start() {
    // function to start a car
    println("Vehicle Started")
  }

  override fun stop() {
    // function to stop a car
    println("Vehicle Stopped")
  }
}

fun main() {
  val car = Cars("Ferrari 812 Superfast", "red", 1525.0, 339.60)
  val motor = Cars("Ducati 1098s", "red", 173.0, 271.0)

  car.displayDetails()
  motor.displayDetails()

  car.start()
  motor.start()
}