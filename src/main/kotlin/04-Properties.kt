// Properties 1 - Declaring Properties

class Address {
  var name: String = "Holmes, Sherlock"
  var street: String = "Baker"
  var city: String = "London"
  var state: String? = null
  var zip: String = "123456"
}
//
fun copyAddress(address: Address): Address {
  val result = Address() // there's no 'new' keyword in Kotlin
  result.name = address.name // accessors are called
  result.street = address.street
  // ...
  return result
}

// Properties 2 - Overriding Properties
open class Shape {
  open val vertexCount: Int = 0
}

class Rectangle : Shape() {
  override val vertexCount = 4
}

interface Shapes {
  val vertexCount: Int
}

class Rectangles(override val vertexCount: Int = 4) : Shapes // Always has 4 vertices

class Polygon : Shapes {
  override var vertexCount: Int = 0  // Can be set to any number later
}