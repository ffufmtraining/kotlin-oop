// Enum Classes 1

//import java.util.function.BinaryOperator
//import java.util.function.IntBinaryOperator
//
//enum class IntArithmetics : BinaryOperator<Int>, IntBinaryOperator {
//  PLUS {
//    override fun apply(t: Int, u: Int): Int = t + u
//  },
//
//  TIMES {
//    override fun apply(t: Int, u: Int): Int = t * u
//  };
//
//  override fun applyAsInt(t: Int, u: Int) = apply(t, u)
//}
//
//fun main() {
//  val a = 13
//  val b = 31
//  for (f in IntArithmetics.values()) {
//    println("$f($a, $b) = ${f.apply(a, b)}")
//  }
//}

// PLUS(13, 31) = 44
// TIMES(13, 31) = 403


// Enum Classes 2

//enum class RGB {
//  RED,
//  GREEN,
//  BLUE
//}
//
//inline fun <reified T : Enum<T>> printAllValues() {
//  print(enumValues<T>().joinToString { it.name })
//}
//
//fun main() {
//  printAllValues<RGB>() // prints RED, GREEN, BLUE
//}