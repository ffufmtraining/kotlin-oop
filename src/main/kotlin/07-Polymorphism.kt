// Static Polymorphism
class MethodOverloading {
  fun area(a:Int):Int {
    return a*a
  }

  fun area(length:Int,height:Int):Int {
    return length*height
  }

  fun area(base:Float,height:Float):Float {
    return (base*height)/2
  }
}

fun run1() {
  var obj=MethodOverloading()
  println("Area of Square="+obj.area(5))
  println("Area of Rectangle="+obj.area(5,4))
  println("Area of Triangle="+obj.area(10.05f,5.5f))
}

// Dynamic Polymorphism
open class Car(var speed:Int) {
  open  fun show() {
    println("car is running on $speed km/hrs")
  }
}

class Mercedes(var mspeed:Int) : Car(mspeed) {
  override fun show() {
    println("Mercedize is running on $mspeed km/hrs")
  }
}

fun run2() {
  var newcar = Mercedes(150);
  println(newcar.show())
}