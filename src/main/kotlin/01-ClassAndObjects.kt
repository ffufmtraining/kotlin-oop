// Class and Objects

class OuterClass {
  var name: String = "Ash"
    get() {
      return field
    }
    set(value) {
      field = value
    }

  fun insideFoo() {
    println(this.name)
  }

  class NestedClass {
    var description: String = "code inside nested class"
    private var id: Int = 101
    fun foo() {
      // print("name is ${name}") // cannot access the outer class member
      println("Id is $id")
    }
  }
}

fun main() {
  val obj1 = OuterClass()

  obj1.insideFoo()
  obj1.name = "Ashley"
  obj1.insideFoo()

  // nested class must be initialize
  // accessing property
  println(OuterClass.NestedClass().description)

  // object creation
  val obj2 = OuterClass.NestedClass()

  // access member function
  obj2.foo()
}