// Extensions 1

//fun MutableList<Int>.swap(index1: Int, index2: Int) {
// val tmp = this[index1] // 'this' corresponds to the list
// this[index1] = this[index2]
// this[index2] = tmp
//}
//
//fun main() {
//  val list = mutableListOf(1, 2, 3)
//  list.swap(0, 2) // 'this' inside 'swap()' will hold the value of 'list'
//}

// Extensions 2 - Generic

//fun <T> MutableList<T>.swap(index1: Int, index2: Int) {
// val tmp = this[index1] // 'this' corresponds to the list
// this[index1] = this[index2]
// this[index2] = tmp
//}


// Extensions 3

//fun main() {
// open class Shape
// class Rectangle: Shape()
//
// fun Shape.getName() = "Shape"
// fun Rectangle.getName() = "Rectangle"
//
// fun printClassName(s: Shape) {
//  println(s.getName())
// }
//
// printClassName(Rectangle())
//}


// Extensions 4

//fun main() {
// class Example {
//  fun printFunctionType() {
//   println("Class method")
//  }
// }
//
// fun Example.printFunctionType() {
//  println("Extension function")
// }
//
// Example().printFunctionType()
//}


// Extensions 5

//fun main() {
// class Example {
//  fun printFunctionType() {
//   println("Class method")
//  }
// }
//
// fun Example.printFunctionType(i: Int) {
//  println("Extension function #$i")
// }
//
// Example().printFunctionType(1)
//}


// Extensions 6 - Nullable Receiver

//fun Any?.toString(): String {
// if (this == null) return "null"
// // after the null check, 'this' is autocast to a non-null type, so the toString() below
// // resolves to the member function of the Any class
// return toString()
//}


// Extensions 6 - Properties

//fun main() {
//  val House.number = 1 // error: initializers are not allowed for extension properties
//}


// Extensions 7 - Declaring Extensions as Members

//class Host(val hostname: String) {
//  fun printHostname() {
//    print(hostname)
//  }
//}
//
//class Connection(val host: Host, val port: Int) {
//  fun printPort() {
//    print(port)
//  }
//
//  fun Host.printConnectionString() {
//    printHostname()   // calls Host.printHostname()
//    print(":")
//    printPort()   // calls Connection.printPort()
//  }
//
//  fun connect() {
//    /*...*/
//    host.printConnectionString()   // calls the extension function
//  }
//}
//
//fun main() {
//  Connection(Host("kotl.in"), 443).connect()
//  // Host("kotl.in").printConnectionString()  // error, the extension function is unavailable outside Connection
//}


// Extensions 8

//open class Base { }
//
//class Derived : Base() { }
//
//open class BaseCaller {
//  open fun Base.printFunctionInfo() {
//    println("Base extension function in BaseCaller")
//  }
//
//  open fun Derived.printFunctionInfo() {
//    println("Derived extension function in BaseCaller")
//  }
//
//  fun call(b: Base) {
//    b.printFunctionInfo()   // call the extension function
//  }
//}
//
//class DerivedCaller: BaseCaller() {
//  override fun Base.printFunctionInfo() {
//    println("Base extension function in DerivedCaller")
//  }
//
//  override fun Derived.printFunctionInfo() {
//    println("Derived extension function in DerivedCaller")
//  }
//}
//
//fun main() {
//  BaseCaller().call(Base())   // "Base extension function in BaseCaller"
//  DerivedCaller().call(Base())  // "Base extension function in DerivedCaller" - dispatch receiver is resolved virtually
//  DerivedCaller().call(Derived())  // "Base extension function in DerivedCaller" - extension receiver is resolved statically
//}


// Base extension function in BaseCaller
// Base extension function in DerivedCaller
// Base extension function in DerivedCaller