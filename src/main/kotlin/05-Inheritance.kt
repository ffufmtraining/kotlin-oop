// Single Inheritance
open class AccountInfo(var name:String,var number:Int) {
  fun publicInfo() {
    println("A/C Name=$name")
    println("A/C Number=$number")
  }
}

class PersonalInfo:AccountInfo("Mani",101005) {
  var email:String="trainermani@gmail.com"
  var contact:Long=8882265032L
  fun showContactInfo() {
    println("Email=$email")
    println("Contact=$contact")
  }
}

fun run1(args:Array<String>) {
  var pi = PersonalInfo()

  pi.publicInfo()
  pi.showContactInfo()
}

// Multilevel Inheritance
open class Employee(private var empid:Int) {
  fun showId() {
    println("Employee Id = $empid")
  }
}

open class AddressInfo(var address:String,var id:Int):Employee(id) {
  fun showAddress() {
    println("Address = $address")
  }
}

open class ContactsInfo(var number:Long,var add:String,var userid:Int):AddressInfo(add,userid) {
  fun showContacts() {
    println("Contact = $number")
  }
}

fun run2(args:Array<String>) {
  var info = ContactsInfo(8882265032,"Delhi",5001)
  info.showId()
  info.showAddress()
  info.showContacts()
}