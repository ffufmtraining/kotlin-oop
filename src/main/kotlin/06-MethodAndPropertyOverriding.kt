// Method and Property Overriding
open class Employees {
  // Use "open" modifier to allow child classes to override this property
  open val baseSalary: Double = 30000.0
}

class Programmer : Employees() {
  // Use "override" modifier to override the property of base class
  override val baseSalary: Double = 50000.0
}

fun main(args: Array<String>) {
  val employee = Employees()
  println(employee.baseSalary) // 30000.0

  val programmer = Programmer()
  println(programmer.baseSalary) // 50000.0
}

// Method and Property Overriding - Using super() keyword
open class Employees2 {
  open val baseSalary: Double = 10000.0

  open fun displayDetails() {
    println("I am an Employee")
  }
}

class Developer: Employees2() {
  override var baseSalary: Double = super.baseSalary + 10000.0

  override fun displayDetails() {
    super.displayDetails()
    println("I am a Developer")
  }
}

// Method and Property Overriding - Using a getter/setter
open class Persons1 {
  open var age: Int = 1
}

class CheckedPerson: Persons1() {
  override var age: Int = 1
    set(value) {
      field = if(value > 0) value else throw IllegalArgumentException("Age can not be negative")
    }
}

fun run(args: Array<String>) {
  val person = Persons1()
  person.age = -5 // Works

  val checkedPerson = CheckedPerson()
  checkedPerson.age = -5  // Throws IllegalArgumentException : Age can not be negative
}